#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <time.h>
#include <fstream>
#include <sstream>

using namespace std;

struct n{
    int NPCkeyX;
    int NPCkeyY;
    int flagX;
    int flagY;
    bool keyBeli;
    bool bukaFlag;
    bool takenFlag;
};

string bentukGold[5];
int posisiGoldX[5], posisiGoldY[5], detikGold = 0;
string nama, peta[60][60], text, input_terakhir;
int gold, zeny, input, xplayer = 30, yplayer = 30, NPCsaveX, NPCsaveY;
n posisi[3];
bool balik = true;

void cetakPeta_awal(){
    for(int i = 0 ; i < 60 ; i++){
        for(int j = 0 ; j < 60 ; j++){
            if(i == yplayer && j == xplayer){
                peta[i][j] = "P";
            }
            else if(i <= 4 || i >= 53 || j <= 4 || j >= 53){
                peta[i][j] = "#";
            }
            else{
                peta[i][j] = " ";
            }
        }
    }
}

void cetakpeta(){
    for(int i = 0 ; i < 50 ; i++){
        for(int j = 0 ; j < 50 ; j++){
            cout << peta[i][j];
        }
        cout << endl;
    }
}

void randomNPCkey(){
    for(int i = 0 ; i < 3 ; i++){
        do{
            posisi[i].NPCkeyX = rand()%47+5;
            posisi[i].NPCkeyY = rand()%47+5;
        }while(peta[posisi[i].NPCkeyY][posisi[i].NPCkeyX] != " ");
        peta[posisi[i].NPCkeyY][posisi[i].NPCkeyX] = "N";
        posisi[i].keyBeli = false;
    }
}

void randomNPCsave(){
    do{
        NPCsaveX = rand()%47+5;
        NPCsaveY = rand()%47+5;
    }while(peta[NPCsaveY][NPCsaveX] != " ");
    peta[NPCsaveY][NPCsaveX] = "S";
}

int check_area(int x, int y){
    bool aman = true;
    for(int i = y-1 ; i <= y+1 ; i++){
        for(int j = x-1 ; j <= x+1 ; j++){
            if(peta[i][j] != " "){
                aman = false;
                i+=3;
            }
        }
    }
    return aman;
}

void cetakFlag(int i){
    for(int a = posisi[i].flagY-1 ; a < posisi[i].flagY+2 ; a++){
        for(int b = posisi[i].flagX-1 ; b < posisi[i].flagX+2 ; b++){
            if(a == posisi[i].flagY && b == posisi[i].flagX && posisi[i].takenFlag == false){
                peta[a][b] = i+49;
            }
            else if(posisi[i].bukaFlag == false){
                peta[a][b] = "#";
            }
        }
    }
}

void cetakNPC(){
    for(int i = 0 ; i < 3 ; i++){
        peta[posisi[i].NPCkeyY][posisi[i].NPCkeyX] = "N";
        cetakFlag(i);
    }
    peta[NPCsaveY][NPCsaveX] = "S";
}


void randomKey(){
    bool aman;
    for(int i = 0 ; i < 3 ; i++){
        do{
            aman = true;
            posisi[i].flagX = rand()%47+5;
            posisi[i].flagY = rand()%47+5;
            aman = check_area(posisi[i].flagX, posisi[i].flagY);
        }while(!aman);
        posisi[i].bukaFlag = false;
        posisi[i].takenFlag = false;
        cetakFlag(i);
    }
}

void loading(string a){
    system("CLS");
    for(int i = 0 ; i < 4 ; i++){
        if(i == 0) cout << a;
        else cout << ".";
        Sleep(500);
    }
}

int check_5x5(int i, int j){
    bool cek = false;
    for(int a = yplayer-2 ; a < yplayer+3 ; a++){
        for(int b = xplayer-2 ; b < xplayer+3 ; b++){
            if(i == a && j == b){
                cek = true;
                a+=yplayer;
            }
        }
    }
    return cek;
}

void cetakPeta_10x10(){
    int x = xplayer-5, y = yplayer-5;
    for(int i = y ; i < yplayer+6 ; i++){
        for(int j = x ; j < xplayer+6 ; j++){
            bool area5 = check_5x5(i,j);
            if(area5 == true) cout << peta[i][j];
            else cout << " ";
        }
        cout << endl;
    }
}

void cetakStatus(){
    cout << "*-----------------*" << endl;
    cout << "  Status Player :" << endl;
    cout << "Key 1  : " << posisi[0].keyBeli << " piece  " << "Nama : " << nama << endl;
    cout << "Key 2  : " << posisi[1].keyBeli << " piece  " << "Gold : " << gold << endl;
    cout << "Key 3  : " << posisi[2].keyBeli << " piece  " << "Zeny : " << zeny << endl;
    cout << "Flag 1 : " << posisi[0].takenFlag << " piece " << endl;
    cout << "Flag 2 : " << posisi[1].takenFlag << " piece " << endl;
    cout << "Flag 3 : " << posisi[2].takenFlag << " piece " << endl;
    cout << "Press H-key to go to help section... " << endl;
    cout << "Press 0-key to help you find the coordinate..." << endl;
    cout << "Press B-key to back to main menu..." << endl;
}

void gerak_player(int x, int y){
    if(peta[y][x] == "X" || peta[y][x] == "." || peta[y][x] == " "){
        peta[y][x] = "P";
        xplayer = x; yplayer = y;
    }
}

void successBuy(int i){
    cout << "+---------------------------------+" << endl;
    cout << "| You got Key " << i+1 << "...                |" << endl;
    cout << "| Now you can unlock flag " << i+1 << " door. |" << endl;
    cout << "+---------------------------------+" << endl;
    posisi[i].keyBeli = true;
    getch();
}

void buyingKey(int i){
    system("CLS");
    if(posisi[i].keyBeli == true){
        cout << "+---------------------------+" << endl;
        cout << "| You already have the key. |" << endl;
        cout << "+---------------------------+" << endl;
        getch();
    }
    else if(i == 0 && zeny >= 300){
        successBuy(i);
        zeny -= 300;
    }
    else if(i == 1 && zeny >= 500){
        successBuy(i);
        zeny -= 500;
    }
    else if(i == 2 && zeny >= 800){
        successBuy(i);
        zeny -= 800;
    }
    else{
        cout << "+-----------------------------+" << endl;
        cout << "| You don't have enough Zeny. |" << endl;
        cout << "+-----------------------------+" << endl;
        getch();
    }
}

void sellGold(){
    system("CLS");
    if(gold == 0){
        cout << "+------------------------------+" << endl;
        cout << "| You don't have gold to sell. |" << endl;
        cout << "+------------------------------+" << endl;
        getch();
    }
    else{
        zeny += gold*50; gold = 0;
        cout << "+-------------------------------------+" << endl;
        cout << "| Gold converted to Zeny successfully |" << endl;
        cout << "+-------------------------------------+" << endl;
        getch();
    }
}

void openShop(int i){
    system("CLS");
    text = "Opening shop";
    loading(text);
    bool a = true;
    while(a){
        system("CLS");
        cout << " * Shop Number " << i+1 << " *" << endl;
        cout << "Zeny : " << zeny << " Zeny" << endl;
        cout << "Gold : " << gold << endl;
        cout << "+-----------------------+" << endl;
        cout << "| 1. Buy KEY ";
        if(i == 0) cout << "<300 zeny> |" << endl;
        else if(i == 1) cout << "<500 zeny> |" << endl;
        else if(i == 2) cout << "<800 zeny> |" << endl;
        cout << "| 2. Sell Gold          |" << endl;
        cout << "| 3. Exit               |" << endl;
        cout << "+-----------------------+" << endl;
        cout << "Choices : "; cin >> input;
        if(input == 1){
            buyingKey(i);
        }
        else if(input == 2){
            sellGold();
        }
        else if(input == 3){
            system("CLS");
            cout << "+---------------------------+" << endl;
            cout << "| Please come back again... |" << endl;
            cout << "+---------------------------+" << endl;
            getch();
            a = false;
        }
    }
}

void checkDoor(int i, int x, int y){
    for(int a = posisi[i].flagY-1 ; a < posisi[i].flagY+2 ; a++){
        for(int b = posisi[i].flagX-1 ; b < posisi[i].flagX+2 ; b++){
            if((peta[y][x] == "#" && y == a && x == b) && posisi[i].bukaFlag == false){
                if(posisi[i].keyBeli == true && peta[y][x] == "#"){
                    cout << "+---------------------------+" << endl;
                    cout << "| Congratulation!!          |" << endl;
                    cout << "| You have opened the door. |" << endl;
                    cout << "+---------------------------+" << endl;
                    posisi[i].bukaFlag = true;
                    getch();
                }
                else if(peta[y][x] == "#"){
                    cout << "+-------------------------------------------+" << endl;
                    cout << "| Door locked.                              |" << endl;
                    cout << "| You must have the key to unlock this door |" << endl;
                    cout << "+-------------------------------------------+" << endl;
                    getch();
                }
                a+=10; b+=10;
            }
        }
    }
}
void loaddataplayer (bool &buka)
{
    string gabung = nama + ".txt";
    int ctr = 0;
    ifstream loadplayer(gabung.c_str());
    if(loadplayer.is_open())
    {
        string line;
        while(ctr == 0)
        {
            getline(loadplayer,line);
            nama = line;
            getline(loadplayer,line);
            stringstream (line) >> zeny;
            getline(loadplayer,line);
            stringstream (line) >> gold;
            getline(loadplayer,line);
            stringstream (line) >> xplayer;
            getline(loadplayer,line);
            stringstream (line) >> yplayer;
            getline(loadplayer,line);
            stringstream (line) >> NPCsaveX;
            getline(loadplayer,line);
            stringstream (line) >> NPCsaveY;
            ctr++;
        }
        loadplayer.close();
    }
    else
    {
        system("CLS");
        cout << "Username tidak ada";
        getch();
        buka = false;
    }
}
void loadposisigold()
{
    string gabung = nama + "loadposisigold.txt";
    int ctr = 0;
    ifstream loadgold(gabung.c_str());
    if(loadgold.is_open())
    {
        string line;
        while(ctr < 5)
        {
            getline(loadgold,line);
            stringstream(line) >> posisiGoldX[ctr];
            getline(loadgold,line);
            stringstream(line) >> posisiGoldY[ctr];
            getline(loadgold,line);
            bentukGold[ctr] = line;
            ctr++;
        }
        loadgold.close();
    }

}
void loaddataNPCkey()
{
    int ctr = 0;
    string gabung = nama+ "loaddataNPCkey.txt";
    ifstream loadNPCkey (gabung.c_str());
    if(loadNPCkey.is_open())
    {
        string line;
        while(ctr < 3)
        {
            getline(loadNPCkey,line);
            stringstream(line) >> posisi[ctr].NPCkeyX;
            getline(loadNPCkey,line);
            stringstream(line) >> posisi[ctr].NPCkeyY;
            ctr++;
        }
        loadNPCkey.close();
    }
}

void loaddataflag()
{
    int ctr = 0;
    string gabung = nama + "loadDataflag.txt";
    ifstream loadflag (gabung.c_str());
    if(loadflag.is_open())
    {
        string line;
        while (ctr < 3)
        {
            getline(loadflag,line);
            stringstream(line) >> posisi[ctr].flagX;
            getline(loadflag,line);
            stringstream(line) >> posisi[ctr].flagY;
            ctr++;
        }
        loadflag.close();
    }
}
void loadbukaflagdankeybeli()
{
    int ctr = 0;
    string gabung = nama + "loadDatabukaflagdankeybeli.txt";
    ifstream loadbukaflagdankeybeli (gabung.c_str());
    if(loadbukaflagdankeybeli.is_open())
    {
        string line;
        while (ctr < 3)
        {
            getline(loadbukaflagdankeybeli,line);
            if (line == "0")
            {
                posisi[ctr].bukaFlag = false;
            }
            else
            {
                posisi[ctr].bukaFlag = true;
            }
            getline(loadbukaflagdankeybeli,line);
            if (line == "0")
            {
                posisi[ctr].keyBeli = false;
            }
            else
            {
                posisi[ctr].keyBeli = true;
            }
            getline(loadbukaflagdankeybeli,line);
            if (line == "0")
            {
                posisi[ctr].takenFlag = false;
            }
            else
            {
                posisi[ctr].takenFlag = true;
            }
            ctr++;
        }
        loadbukaflagdankeybeli.close();
    }
}

void save ()
{
    string gabungdataplayer = nama + ".txt";
    ofstream savedataplayer (gabungdataplayer.c_str());
    if(savedataplayer.is_open())
    {
        savedataplayer << nama << endl;
        savedataplayer << zeny << endl;
        savedataplayer << gold << endl;
        savedataplayer << xplayer << endl;
        savedataplayer << yplayer << endl;
        savedataplayer << NPCsaveX << endl;
        savedataplayer << NPCsaveY << endl;
        savedataplayer.close();
    }
    string gabungposisigold = nama + "loadposisigold.txt";
    ofstream saveposisigold (gabungposisigold.c_str());
    int ctrposisigold = 0;
    if(saveposisigold.is_open())
    {
        while(ctrposisigold < 5)
        {
            saveposisigold << posisiGoldX[ctrposisigold]<< endl;
            saveposisigold << posisiGoldY[ctrposisigold]<< endl;
            saveposisigold << bentukGold[ctrposisigold]<< endl;
            ctrposisigold++;
        }
        saveposisigold.close();
    }
    string gabungNPCkey = nama + "loaddataNPCkey.txt";
    int ctrNPCKey = 0;
    ofstream saveNPCKey (gabungNPCkey.c_str());
    if(saveNPCKey.is_open())
    {
        while(ctrNPCKey < 3)
        {
            saveNPCKey << posisi[ctrNPCKey].NPCkeyX << endl;
            saveNPCKey << posisi[ctrNPCKey].NPCkeyY << endl;
            ctrNPCKey++;
        }
        saveNPCKey.close();
    }
    string gabungdataflag = nama + "loadDataflag.txt";
    int ctrdataflag = 0;
    ofstream savedataflag (gabungdataflag.c_str());
    if(savedataflag.is_open())
    {
        while(ctrdataflag < 3)
        {
            savedataflag << posisi[ctrdataflag].flagX << endl;
            savedataflag << posisi[ctrdataflag].flagY << endl;
            ctrdataflag++;
        }
        savedataflag.close();
    }
    string gabungbukaflagdankeybeli = nama + "loadDatabukaflagdankeybeli.txt";
    int ctrbukaflagdankeybeli = 0;
    ofstream savebukaflagdankeybeli (gabungbukaflagdankeybeli.c_str());
    if(savebukaflagdankeybeli.is_open())
    {
        while(ctrbukaflagdankeybeli < 3)
        {
            savebukaflagdankeybeli << posisi[ctrbukaflagdankeybeli].bukaFlag << endl;
            savebukaflagdankeybeli << posisi[ctrbukaflagdankeybeli].keyBeli << endl;
            savebukaflagdankeybeli << posisi[ctrbukaflagdankeybeli].takenFlag << endl;
            ctrbukaflagdankeybeli++;
        }
        savebukaflagdankeybeli.close();
    }
    system("CLS");
    cout << "+------------+" << endl;
    cout << "|Data saved. |" << endl;
    cout << "+------------+" << endl;
    getch();
}

void TalkOpen(int x, int y){
    if(x == NPCsaveX && y == NPCsaveY){
        save();
    }
    for(int i = 0 ; i < 3 ; i++){
        checkDoor(i,x,y);
        if(x == posisi[i].NPCkeyX && y == posisi[i].NPCkeyY){
            openShop(i);
        }
        else if(x == posisi[i].flagX && y == posisi[i].flagY && posisi[i].takenFlag == false){
            posisi[i].takenFlag = true;
            cout << "+--------------------------------------+" << endl;
            cout << "| Congratulation!!                     |" << endl;
            cout << "| You have successfully taken flag " << i+1 << "!! |" << endl;
            cout << "+--------------------------------------+" << endl;
            getch();
        }
    }
}

void gerak_playerSpasi(){
    if(input_terakhir == "w") TalkOpen(xplayer,yplayer-1);
    else if(input_terakhir == "s") TalkOpen(xplayer,yplayer+1);
    else if(input_terakhir == "a") TalkOpen(xplayer-1,yplayer);
    else if(input_terakhir == "d") TalkOpen(xplayer+1,yplayer);
}

void menggali(){
    for(int i = 0 ; i < 5 ; i++){
        if(xplayer == posisiGoldX[i] && yplayer == posisiGoldY[i] && bentukGold[i] == "."){
            bentukGold[i] = "X";
            gold+=1;
        }
    }
}

void gerakHelp(){
    system("CLS");
    cout << "             -- HELP SECTION -- " << endl;
    cout << "+--------------------------------------------+" << endl;
    cout << "| W-key     -> Going - up                    |" << endl;
    cout << "| S-key     -> Going - down                  |" << endl;
    cout << "| A-key     -> Going - left                  |" << endl;
    cout << "| D-key     -> Going - right                 |" << endl;
    cout << "| Space-key -> Talk / Open door / Seize flag |" << endl;
    cout << "| F-key     -> Take Gold                     |" << endl;
    cout << "| H-key     -> Help Section                  |" << endl;
    cout << "+--------------------------------------------+" << endl;
    cout << "Press any key to exit help section...";
    getch();
}

void koordinat(){
    system("CLS");
    cout << "-- COORDINATE SECTION --" << endl;
    cout << "+-------------------------------+" << endl;
    cout << "| Player coordinate = (" << xplayer << "," << yplayer << ")   " << endl;
    cout << "| NPC save coordinate = (" << NPCsaveX << "," << NPCsaveY << ") " << endl;
    cout << "| NPC Key1 coordinate = (" << posisi[0].NPCkeyX << "," << posisi[0].NPCkeyY << ") " << endl;
    cout << "| NPC Key2 coordinate = (" << posisi[1].NPCkeyX << "," << posisi[1].NPCkeyY << ") " << endl;
    cout << "| NPC Key3 coordinate = (" << posisi[2].NPCkeyX << "," << posisi[2].NPCkeyY << ") " << endl;
    cout << "| Flag 1 coordinate   = (" << posisi[0].flagX << "," << posisi[0].flagY << ") " << endl;
    cout << "| Flag 2 coordinate   = (" << posisi[1].flagX << "," << posisi[1].flagY << ") " << endl;
    cout << "| Flag 3 coordinate   = (" << posisi[2].flagX << "," << posisi[2].flagY << ") " << endl << endl;
    cout << "Press any key to exit coordinate section...";
    getch();
}

void check_input(){
    string gerak;
    if(kbhit()){
        gerak = getch();
        if(gerak == "w" || gerak == "W") {gerak_player(xplayer,yplayer-1); input_terakhir = "w";}
        else if(gerak == "a" || gerak == "A") {gerak_player(xplayer-1,yplayer); input_terakhir = "a";}
        else if(gerak == "s" || gerak == "S") {gerak_player(xplayer,yplayer+1); input_terakhir = "s";}
        else if(gerak == "d" || gerak == "D") {gerak_player(xplayer+1,yplayer); input_terakhir = "d";}
        else if(gerak == "h" || gerak == "H") gerakHelp();
        else if(gerak == "f" || gerak == "F") menggali();
        else if(gerak == "0") koordinat();
        else if(gerak == "B" || gerak == "b") {
            system("CLS");
            string inputan;
            cout << "Do you want to back to main menu (Y/N)";
            inputan = getch();
            if(inputan == "Y" || inputan == "y"){
                text = "Loading to main menu";
                loading(text);
                balik = false;
            }
        }
        else if(gerak == " ") {gerak_playerSpasi();}
    }
}

void randomGold(){
    for(int i = 0 ; i < 5 ; i++){
        posisiGoldY[i] = 0;
        posisiGoldX[i] = i;
        bentukGold[i] = ".";
        peta[posisiGoldY[i]][posisiGoldX[i]] = bentukGold[i];
    }
}

void checkGold(){
    for(int i = 0 ; i < 5 ; i++){
        if(posisiGoldX[i] > xplayer+5 || posisiGoldX[i] < xplayer-5 || posisiGoldY[i] > yplayer+5 || posisiGoldY[i] < yplayer-5){
            int xawal = xplayer-5, xakhir = xplayer+6 - xawal;
            int yawal = yplayer-5, yakhir = yplayer+6 - yawal;
            bentukGold[i] = ".";
            do{
                posisiGoldX[i] = rand()%xakhir + xawal;
                posisiGoldY[i] = rand()%yakhir + yawal;
            }while(peta[posisiGoldY[i]][posisiGoldX[i]] != " ");
        }
    }
}

void cetakGold(){
    for(int i = 0 ; i < 5 ; i++){
        if(bentukGold[i] == "X"){
            peta[posisiGoldY[i]][posisiGoldX[i]] = bentukGold[i];
            if(detikGold == 100) detikGold = 0;
        }
        else if(detikGold >= 50 && detikGold <= 100){
            peta[posisiGoldY[i]][posisiGoldX[i]] = bentukGold[i];
            if(detikGold == 100) detikGold = 0;
        }
        else if(detikGold < 50){
            peta[posisiGoldY[i]][posisiGoldX[i]] = " ";
        }
    }
}

void perubahan_peta(){
    detikGold++;
    cetakPeta_awal();
    cetakNPC();
    checkGold();
    cetakGold();
    peta[yplayer][xplayer] = "P";
}

void gameplay(){
    while(balik){
        system("CLS");
        cetakPeta_10x10();
        check_input();
        cetakStatus();
        perubahan_peta();
    }
}

void start(){
    srand(time(NULL));
    cout << "-- NEW GAME --" << endl;
    cin.ignore();
    cout << "Masukkan nama : "; getline(cin,nama);
    zeny = 0; gold = 0;
    detikGold = 0;
    cetakPeta_awal();
    randomNPCkey();
    randomNPCsave();
    randomKey();
    randomGold();
    text = "Game loading";
    loading(text);
    gameplay();
}

void load(){
    bool buka = true;
    text = "Initiate load";
    loading(text);
    system("CLS");
    cout << " -- LOAD SECTION --" << endl;
    cout << "---------------------" << endl;
    cin.ignore();
    cout << "What is your load name?"; getline(cin,nama);
    detikGold = 0;
    loaddataplayer(buka);
    if(buka == true)
    {
        loadposisigold();
        loaddataNPCkey();
        loaddataflag();
        loadbukaflagdankeybeli();
        gameplay();
    }
}

void menu(){
    while(true){
        system("CLS");
        cout << "1. Start" << endl;
        cout << "2. Load" << endl;
        cout << "Pilihan : "; cin >> input;
        system("CLS");
        balik = true;
        if(input == 1){
            text = "Please wait";
            loading(text);
            system("CLS");
            start();
        }
        else if(input == 2){
            load();
        }
    }
}

int main ()
{
    menu();
    return 0;
}
